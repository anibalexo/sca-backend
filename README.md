olume# Control and Assistance (Backend)

SCA is an app for employee control and permission management

- **Python (3.9)**
- **[Django (3.2.13)](https://www.djangoproject.com/)**: framework to build the backend
- **[Django Rest Framework 3.13.1](https://www.django-rest-framework.org/)**: powerful and flexible toolkit for building Web APIs.
- **[drf-spectacular (0.22.1)](https://drf-spectacular.readthedocs.io/en/latest/index.html)**: customize and flexible OpenAPI 3.0 schema generation for Django REST framework.

### Plugins for Testing

- **[Pytest (7.1.2)](https://docs.pytest.org/en/7.1.x/)**: makes it easy to write small, readable tests
- **[pytest-django (4.5.2)](https://pytest-django.readthedocs.io/en/latest/)**: plugin for pytest that provides a set of useful tools for testing Django applications and projects.
- **[pytest-cov (3.0.0)](https://github.com/pytest-dev/pytest-cov)**: this plugin produces coverage reports.
- **[model-bakery (1.6.0)](https://model-bakery.readthedocs.io/en/latest/)**: offers you a smart way to create fixtures for testing in Django.

### Development Environment
Create a directory for environment variables with the name `.env` the directory must be in the root of the project and the file with the variables with the name `.env_dev`, in this file the following variables must be defined:

```env
#Django secret key and settings file for docker compose
DJANGO_SETTINGS_MODULE=djangoserver.settings.local
DJANGO_SECRET_KEY='[secret key django]'

#Postgresql access credentials
POSTGRES_DB=[name database]
POSTGRES_USER=[user database]
POSTGRES_PASSWORD=[password database]
POSTGRES_HOST=[ip database]
POSTGRES_PORT=[port database]
```

Create a volume to persistently store postgres database data with the following command:
```sh
$ docker volume create --name db_data_sca -d local
```

Build the image and run the development project, with the commands:
```sh
$ docker-compose -f local.yml build
$ docker-compose -f local.yml up -d
```

Execute the model migrations to the postgres base, execute the commands on the container:
```sh
$ docker-compose exec -it <id contenedor> sh
# python manage.py makemigrations
# python manage.py migrate
```

To force build images without using cache, run the command:
```sh
$ docker-compose -f local.yml build --no-cache
```


### Test Environment
For the test environment you need to create a file called `.env_test` in the `.env` directory, in this file define the following variables:
```env
#Django secret key and settings file for docker compose
DJANGO_SETTINGS_MODULE=djangoserver.settings.test
DJANGO_SECRET_KEY='[secret key django]'

#Postgresql access credentials
POSTGRES_DB=[name database]
POSTGRES_USER=[user database]
POSTGRES_PASSWORD=[password database]
POSTGRES_HOST=[ip database]
POSTGRES_PORT=[port database]
```

To run in a test environment, run:
```sh
$ docker-compose -f tests.yml build
$ docker-compose -f tests.yml run --rm djangoserver_test
```

### Production Environment
...