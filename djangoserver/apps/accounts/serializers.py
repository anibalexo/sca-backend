from drf_spectacular.utils import OpenApiResponse, inline_serializer
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):

    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)
        data['uid'] = self.user.pk
        data['first_name'] = self.user.first_name
        data['last_name'] = self.user.last_name
        return data

    @staticmethod
    def get_response_success_openapi():
        return OpenApiResponse(
            response=inline_serializer(
                name='ResponseCustomTokenObtainPairSerializer',
                fields={
                    'refresh': serializers.CharField(
                        required=False, help_text='When access token expires, you can use the longer-lived '
                                                  'refresh token to obtain another access token'
                    ),
                    'access': serializers.CharField(
                        required=False, help_text='Token to consume endpoints, must be added in headers'
                    ),
                    'uid': serializers.IntegerField(
                        required=False,
                        help_text='Id user'
                    ),
                    'first_name': serializers.CharField(
                        required=False,
                        help_text='Name User'
                    ),
                    'last_name': serializers.CharField(
                        required=False,
                        help_text='Last Name User'
                    )
            }),
            description='Successful Operation (Token JWT created)'
        )
