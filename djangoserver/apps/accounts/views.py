from drf_spectacular.utils import extend_schema
from rest_framework_simplejwt.views import TokenObtainPairView

from .serializers import CustomTokenObtainPairSerializer
from ..api.utils.custom_responses_openapi import response400, response401


@extend_schema(
    description="Takes a set of user credentials and returns object "
                "JSON with an token access and user data.",
    request=CustomTokenObtainPairSerializer,
    responses={
        200: CustomTokenObtainPairSerializer.get_response_success_openapi(),
        400: response400(),
        401: response401(),
    },
    methods=["POST"]
)
class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer

