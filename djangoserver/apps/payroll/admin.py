from django.contrib import admin

from .models import (
    CivilStatus, PermissionType, Position, Department, Agency, Employee,
    Permission, Vacation
)


class CivilStatusAdmin(admin.ModelAdmin):
    list_display = ('pk','name', 'creation_date', 'last_update',)
    search_fields = ('name',)
    list_per_page = 20
    readonly_fields = ('creation_date', 'last_update')

class PermissionTypeAdmin(admin.ModelAdmin):
    list_display = ('pk','name', 'erp_permission_type_code', 'creation_date',
                    'last_update',)
    search_fields = ('name', 'erp_permission_type_code')
    list_per_page = 20
    readonly_fields = ('creation_date', 'last_update')

class PositionTypeAdmin(admin.ModelAdmin):
    list_display = ('pk','name', 'erp_position_code', 'creation_date', 'last_update',)
    search_fields = ('name', 'erp_position_code')
    list_per_page = 20
    readonly_fields = ('creation_date', 'last_update')

class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('pk','name', 'location', 'erp_department_code', 'creation_date',
                    'last_update',)
    search_fields = ('name', 'location', 'erp_department_code')
    list_per_page = 20
    readonly_fields = ('creation_date', 'last_update')

class AgencyAdmin(admin.ModelAdmin):
    list_display = ('pk','name', 'description', 'reference', 'erp_agency_code',
                    'creation_date', 'last_update',)
    search_fields = ('name', 'description', 'reference', 'erp_agency_code')
    list_per_page = 20
    readonly_fields = ('creation_date', 'last_update')

class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('pk','first_name', 'last_name', 'active', 'position', 'agency',
                    'department',)
    search_fields = ('first_name', 'last_name', 'position__name', 'agency__name',
                     'department__name')
    list_filter = ('active', 'agency', 'position', 'department')
    filter_horizontal = ('devices',)
    list_per_page = 20
    readonly_fields = ('creation_date', 'last_update')

class PermissionAdmin(admin.ModelAdmin):
    list_display = ('employee', 'permission_type', 'measure_time', 'exit_date',
                    'entry_date', 'exit_time', 'entry_time')
    search_fields = ('employee__first_name', 'employee__last_name', 'permission_type__name')
    list_filter = ('permission_type', 'measure_time', 'justified', 'employee')
    list_per_page = 20
    readonly_fields = ('creation_date', 'last_update')

class VacationAdmin(admin.ModelAdmin):
    list_display = ('employee', 'paid', 'exit_date', 'entry_date')
    search_fields = ('employee__first_name', 'employee__last_name')
    list_filter = ('paid', 'employee')
    list_per_page = 20
    readonly_fields = ('creation_date', 'last_update')

admin.site.register(CivilStatus, CivilStatusAdmin)
admin.site.register(PermissionType, PermissionTypeAdmin)
admin.site.register(Position, PositionTypeAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(Agency, AgencyAdmin)
admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Permission, PermissionAdmin)
admin.site.register(Vacation, VacationAdmin)
