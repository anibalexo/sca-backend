from django.db import models


class Position(models.Model):
    name = models.CharField(max_length=80, unique=True, help_text='Name position')
    erp_position_code = models.CharField(max_length=20, unique=True, blank=True, null=True,
                                         help_text='Code position on ERP')
    creation_date = models.DateTimeField(auto_now_add=True, help_text='Date the record was created')
    last_update = models.DateTimeField(auto_now=True, help_text='Date of last update of the registry')

    class Meta:
        verbose_name = 'position'
        verbose_name_plural = 'positions'
        ordering = ['-name']

    def __str__(self):
        return "{}".format(self.name)
