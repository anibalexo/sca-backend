from django.db import models


class Employee(models.Model):
    dni = models.CharField(max_length=15, unique=True,
                           help_text='ID provided by the country to the employee')
    first_name = models.CharField(max_length=80, help_text='Employee first name')
    last_name = models.CharField(max_length=80, help_text='Employee last name')
    address = models.CharField(max_length=150, help_text='Address where the employee lives')
    phone = models.CharField(max_length=25, blank=True, null=True)
    mail = models.EmailField(max_length=254, blank=True, null=True)
    residence = models.CharField(max_length=150, blank=True, null=True,
                                 help_text='City where the employee lives')
    birth_date = models.DateField(help_text='Employee date of birth')
    active = models.BooleanField(default=True,
                                 help_text='Attribute that defines if employee is active in payroll')
    pay_extra = models.BooleanField(default=False,
                                    help_text='Attribute that defines if employee is paid overtime')
    pay_nightly_extra = models.BooleanField(
        default=False, help_text='Attribute that defines if employee is paid nightly overtime'
    )
    privilege = models.CharField(max_length=10, blank=True, null=True)
    device_employee_code = models.IntegerField(default=0, help_text='Employee code in biometric device')
    erp_employee_code = models.CharField(max_length=20, unique=True, blank=True, null=True,
                                         help_text='Employee code in ERP')
    civil_status = models.ForeignKey('CivilStatus', on_delete=models.CASCADE,
                                     help_text='Employee\'s marital status')
    position = models.ForeignKey('Position', on_delete=models.CASCADE,
                                 help_text='Employee position in company')
    agency = models.ForeignKey('Agency', on_delete=models.CASCADE, related_name='employees_agency',
                               help_text='Agency of the company to which the employee belongs')
    department = models.ForeignKey('Department', on_delete=models.CASCADE,
                                    related_name='employees_deparment',
                                    help_text='Department of the company to which the employee belongs')
    devices = models.ManyToManyField('devices.Device', blank=True,
                                     related_name='employees_devices',
                                     help_text='Devices in which the employee is enrolled')
    creation_date = models.DateTimeField(auto_now_add=True, help_text='Date the record was created')
    last_update = models.DateTimeField(auto_now=True, help_text='Date of last update of the registry')

    class Meta:
        verbose_name = 'employee'
        verbose_name_plural = 'employees'
        ordering = ['-id']

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

    @property
    def full_name(self):
        return "{} {}".format(self.first_name, self.last_name)
