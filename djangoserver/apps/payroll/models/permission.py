from django.db import models

from .absence import Absence

HOURS = 'H'
DAYS = 'D'

MEASURE_TIME = (
    (HOURS, 'Hours'),
    (DAYS, 'Days')
)

class Permission(Absence):
    application_date = models.DateTimeField(help_text='Date request')
    reason = models.CharField(max_length=150, help_text='Comments')
    measure_time = models.CharField(max_length=1, choices=MEASURE_TIME, default=HOURS,
                                    help_text='Measure of time for permission: (H) Hours, (D) Days')
    estimated_time = models.TimeField(blank=True, null=True, help_text='Length of time in permission')
    justified = models.BooleanField(default=False, help_text='True if permission is justified')
    erp_permission_code = models.CharField(max_length=20, unique=True, blank=True, null=True,
                                           help_text='Code permission on ERP')
    exit_time = models.TimeField(blank=True, null=True, help_text='Departure time for permission')
    entry_time = models.TimeField(blank=True, null=True, help_text='Return time for permission')
    permission_type = models.ForeignKey('PermissionType', on_delete=models.CASCADE,
                                        help_text='Type of permission')
    employee = models.ForeignKey('Employee', on_delete=models.CASCADE, related_name='permissions',
                                 help_text='Employee linked to leave')

    class Meta:
        verbose_name = 'permission'
        verbose_name_plural = 'permissions'
        ordering = ['-id']

    def __str__(self):
        return "{} - {}".format(self.reason, self.employee)
