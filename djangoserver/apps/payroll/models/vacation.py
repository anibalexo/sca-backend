from django.db import models

from .absence import Absence


class Vacation(Absence):
    paid = models.BooleanField(default=False, help_text='True if vacation is paid')
    erp_vacation_code = models.CharField(max_length=20, unique=True, blank=True, null=True,
                                         help_text='Code vacation on ERP')
    employee = models.ForeignKey('Employee', on_delete=models.CASCADE, related_name='vacations',
                                 help_text='Employee id')

    class Meta:
        verbose_name = 'vacation'
        verbose_name_plural = 'vacations'
        ordering = ['-id']

    def __str__(self):
        return "{}".format(self.employee)