from django.db import models


class Agency(models.Model):
    name = models.CharField(max_length=80, unique=True, help_text='Name for the agency')
    description = models.CharField(max_length=150, help_text='Oservations about the agency')
    reference = models.CharField(max_length=150, help_text='Comments about location')
    erp_agency_code = models.CharField(max_length=20, unique=True, blank=True, null=True,
                                       help_text='Code agency on ERP')
    creation_date = models.DateTimeField(auto_now_add=True, help_text='Date the record was created')
    last_update = models.DateTimeField(auto_now=True, help_text='Date of last update of the registry')

    class Meta:
        verbose_name = 'agency'
        verbose_name_plural = 'agencies'
        ordering = ['-name']

    def __str__(self):
        return "{}".format(self.name)
