from django.db import models


class CivilStatus(models.Model):
    name = models.CharField(max_length=80, unique=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'civil status'
        verbose_name_plural = 'civil status'
        ordering = ['-name']

    def __str__(self):
        return "{}".format(self.name)
