from django.db import models


class Absence(models.Model):
    exit_date = models.DateField(blank=True, null=True, help_text='Date of leave of absence')
    entry_date = models.DateField(blank=True, null=True, help_text='Date of return from absence')
    observations = models.TextField(blank=True, null=True, help_text='Additional comments')
    creation_date = models.DateTimeField(auto_now_add=True, help_text='Date the record was created')
    last_update = models.DateTimeField(auto_now=True, help_text='Date of last update of the registry')

    class Meta:
        abstract = True