from django.db import models


class Department(models.Model):
    name = models.CharField(max_length=80, unique=True, help_text='Name department')
    location = models.CharField(max_length=150, help_text='Comments about location')
    erp_department_code = models.CharField(max_length=20, unique=True, blank=True, null=True,
                                           help_text='Code department on ERP')
    creation_date = models.DateTimeField(auto_now_add=True, help_text='Date the record was created')
    last_update = models.DateTimeField(auto_now=True, help_text='Date of last update of the registry')

    class Meta:
        verbose_name = 'department'
        verbose_name_plural = 'departments'
        ordering = ['-name']

    def __str__(self):
        return "{} - {}".format(self.name, self.location)
