from django.urls import include, path
from drf_spectacular.views import (
    SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView
)
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenRefreshView

from .devices.attendance.views import AttendanceViewSet
from .devices.device.views import DeviceViewSet
from .devices.record.views import RecordBulkListCreateView
from .payroll.agency.views import AgencyViewSet
from .payroll.civil_status.views import CivilStatusViewSet
from .payroll.department.views import DepartmentViewSet
from .payroll.position.views import PositionViewSet
from .payroll.permission_type.views import PermissionTypeViewSet
from .payroll.vacation.views import VacationViewSet
from .payroll.permission.views import PermissionViewSet
from .payroll.employee.views import EmployeeViewSet
from ..accounts.views import CustomTokenObtainPairView

router = DefaultRouter()
# Urls app devices
router.register(r'device', DeviceViewSet, basename='device')
router.register(r'attendance', AttendanceViewSet, basename='attendance')
# Urls app payroll
router.register(r'civilstatus', CivilStatusViewSet, basename='civil-status')
router.register(r'agency', AgencyViewSet, basename='agency')
router.register(r'department', DepartmentViewSet, basename='department')
router.register(r'position', PositionViewSet, basename='position')
router.register(r'permissiontype', PermissionTypeViewSet, basename='permission-type')
router.register(r'vacation', VacationViewSet, basename='vacation')
router.register(r'permission', PermissionViewSet, basename='permission')
router.register(r'employee', EmployeeViewSet, basename='employee')


urlpatterns = [
    path(r'', include((router.urls, 'api'))),
    # url record bulk create
    path(r'record/', RecordBulkListCreateView.as_view(), name="record"),
    # Auth Token
    path(r'auth/token/', CustomTokenObtainPairView.as_view(), name="auth-token"),
    path(r'auth/token/refresh/', TokenRefreshView.as_view(), name="auth-token-refresh"),
    # Schema Api
    path('schema/', SpectacularAPIView.as_view(), name='schema'),
    # Optional UI:
    #path('schema/swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    path('schema/redoc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),
]
