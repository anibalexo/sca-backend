from drf_spectacular.utils import OpenApiResponse, inline_serializer
from rest_framework import serializers

def response400():
    return OpenApiResponse(
        response= inline_serializer(
            name='Response400Custom',
            fields={
                'detail': serializers.CharField(required=False)
            }
        ),
        description='Bad Request'
    )

def response401():
    return OpenApiResponse(
        response= inline_serializer(
            name='Response401Custom',
            fields={
                'detail': serializers.CharField(required=False)
            }
        ),
        description='Bad Credentials'
    )
