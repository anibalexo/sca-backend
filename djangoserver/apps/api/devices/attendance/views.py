from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from .serializers import (
    AttendanceSerializer, ListAttendanceSerializer
)
from ....devices.models import Attendance


@extend_schema_view(
    retrieve=extend_schema(
        description='Detail attendance records'
    ),
    partial_update=extend_schema(
        description='Partial update attendance records'
    ),
    destroy=extend_schema(
        description='Delete attendance records'
    ),
    update=extend_schema(
        description='Update an attendance record'
    ),
    create=extend_schema(
        description='Create an attendance record'
    )
)
class AttendanceViewSet(viewsets.ModelViewSet):
    queryset = Attendance.objects.all()
    authentication_classes = (JWTTokenUserAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = AttendanceSerializer

    @extend_schema(
        description='List attendance records, paginated results with 100 records per page',
        responses=ListAttendanceSerializer
    )
    def list(self, request, *args, **kwargs):
        if bool(request.query_params):
            queryset = self.get_queryset()
            if "page" in request.query_params:
                queryset = self.paginate_queryset(queryset)
                serializer = ListAttendanceSerializer(queryset, many=True, context={"request": request})
                return self.get_paginated_response(serializer.data)
