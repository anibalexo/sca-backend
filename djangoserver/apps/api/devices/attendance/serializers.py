from rest_framework import serializers

from ....devices.models import Attendance, Device
from ....payroll.models import Employee


class AttendanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Attendance
        fields = (
            'id',
            'date',
            'type',
            'employee',
            'device',
            'creation_date',
            'last_update'
        )


class EmployeeNestedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = (
            'id',
            'first_name',
            'last_name'
        )

class DeviceNestedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = (
            'id',
            'name',
            'location'
        )

class ListAttendanceSerializer(AttendanceSerializer):
    employee = EmployeeNestedSerializer()
    device = DeviceNestedSerializer()
