from drf_spectacular.utils import extend_schema
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from .serializers import (
    ListRecordSerializer, BulkRecordSerializer
)
from ....devices.models import Record


class RecordBulkListCreateView(generics.ListCreateAPIView):
    queryset = Record.objects.all()
    authentication_classes = (JWTTokenUserAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = BulkRecordSerializer

    def get_serializer(self, *args, **kwargs):
        if isinstance(kwargs.get("data", {}), list):
            kwargs["many"] = True
        return super(RecordBulkListCreateView, self).get_serializer(*args, **kwargs)

    @extend_schema(
        description='List records, paginated results with 100 records per page',
        responses=ListRecordSerializer
    )
    def get(self, request, *args, **kwargs):
        if bool(request.query_params):
            queryset = self.get_queryset()
            if "page" in request.query_params:
                queryset = self.paginate_queryset(queryset)
                serializer = ListRecordSerializer(queryset, many=True, context={"request": request})
                return self.get_paginated_response(serializer.data)

    @extend_schema(
        description='Create records with method bulk',
        request= BulkRecordSerializer(many=True),
        responses={
            201: None
        }
    )
    def post(self, request, *args, **kwargs):
        return super(RecordBulkListCreateView, self).post(request, *args, **kwargs)
