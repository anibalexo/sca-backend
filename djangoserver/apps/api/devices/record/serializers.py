from rest_framework import serializers

from ....devices.models import Record, Device


class RecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Record
        fields = (
            'id',
            'uid',
            'user_id',
            'date',
            'status',
            'punch',
            'used',
            'device',
            'creation_date',
            'last_update'
        )


class DeviceNestedInRecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = (
            'id',
            'name',
            'ip_address'
        )


class ListRecordSerializer(RecordSerializer):
    device = DeviceNestedInRecordSerializer()


class BulkListRecordSerializer(serializers.ListSerializer):
    """
    We will use the Django’s bulk_create. This function
    allows you to perform a bulk create in the database
    by passing a list of objects.
    """
    def create(self, validated_data):
        result = [self.child.create(attrs) for attrs in validated_data]
        return self.child.Meta.model.objects.bulk_create(result)


class BulkRecordSerializer(serializers.ModelSerializer):
    """
    Modified the serializer so that it no longer does a
    save on the create method, but only returns the new
    instances.

    Then after we have created all of the new instances,
    our BulkListRecordSerializer will call the bulk_create
    method for hits the database a single time to perform
    the creation.
    """
    def create(self, validated_data):
        return Record(**validated_data)

    class Meta:
        model = Record
        fields = (
            'uid',
            'user_id',
            'date',
            'status',
            'punch',
            'used',
            'device',
        )
        read_only_fields = (
            'id',
            'creation_date',
            'last_update'
        )
        list_serializer_class = BulkListRecordSerializer

