from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from .serializers import (
    DeviceSerializer, ListDeviceSerializer
)
from ....devices.models import Device


@extend_schema_view(
    retrieve=extend_schema(
        description='Detail device'
    ),
    update=extend_schema(
        description='Update device info'
    ),
    partial_update=extend_schema(
        description='Partial update device info'
    ),
    destroy=extend_schema(
        description='Delete device'
    ),
)
class DeviceViewSet(viewsets.ModelViewSet):
    queryset = Device.objects.all()
    authentication_classes = (JWTTokenUserAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = DeviceSerializer

    @extend_schema(
        description='List device records, paginated results with 100 records per page',
        responses=ListDeviceSerializer
    )
    def list(self, request, *args, **kwargs):
        if bool(request.query_params):
            queryset = self.get_queryset()
            if "page" in request.query_params:
                queryset = self.paginate_queryset(queryset)
                serializer = ListDeviceSerializer(queryset, many=True, context={"request": request})
                return self.get_paginated_response(serializer.data)
