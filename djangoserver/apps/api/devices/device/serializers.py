from rest_framework import serializers

from ....devices.models import Device
from ....payroll.models import Agency


class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = (
            'id',
            'name',
            'location',
            'ip_address',
            'port',
            'active',
            'code',
            'firmware',
            'name_device',
            'serial',
            'mac',
            'plataform',
            'number_records',
            'capacity_records',
            'agency',
            'creation_date',
            'last_update'
        )


class AgencyNestedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Agency
        fields = (
            'id',
            'name'
        )


class ListDeviceSerializer(DeviceSerializer):
    agency = AgencyNestedSerializer()
