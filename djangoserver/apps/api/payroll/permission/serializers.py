from rest_framework import serializers

from ....payroll.models import Permission, Employee, PermissionType


class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = (
            'id',
            'application_date',
            'reason',
            'measure_time',
            'estimated_time',
            'justified',
            'erp_permission_code',
            'exit_time',
            'entry_time',
            'permission_type',
            'employee',
            'exit_date',
            'entry_date',
            'observations',
            'creation_date',
            'last_update'
        )

class EmployeeNestedPermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = (
            'id',
            'first_name',
            'last_name'
        )

class PermissionTypeNestedPermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = PermissionType
        fields = (
            'id',
            'name',
            'erp_permission_type_code'
        )

class ListPermissionSerializer(PermissionSerializer):
    employee = EmployeeNestedPermissionSerializer()
    permission_type = PermissionTypeNestedPermissionSerializer()
