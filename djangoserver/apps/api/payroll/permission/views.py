from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from .serializers import PermissionSerializer, ListPermissionSerializer
from ....payroll.models import Permission


@extend_schema_view(
    create=extend_schema(
        description='Create permission records'
    ),
    update=extend_schema(
        description='Update permission records'
    ),
    partial_update=extend_schema(
        description='Partial update permission records'
    ),
    destroy=extend_schema(
        description='Delete permission records'
    ),
)
class PermissionViewSet(viewsets.ModelViewSet):
    queryset = Permission.objects.all()
    authentication_classes = (JWTTokenUserAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = PermissionSerializer

    @extend_schema(
        description='List permission records, paginated results with 100 records per page',
        responses=ListPermissionSerializer
    )
    def list(self, request, *args, **kwargs):
        if bool(request.query_params):
            queryset = self.get_queryset()
            if "page" in request.query_params:
                queryset = self.paginate_queryset(queryset)
                serializer = ListPermissionSerializer(queryset, many=True, context={"request": request})
                return self.get_paginated_response(serializer.data)

    @extend_schema(
        description='Detail permission record',
        responses=ListPermissionSerializer
    )
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = ListPermissionSerializer(instance=instance)
        return Response(serializer.data)
