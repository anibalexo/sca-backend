from rest_framework import serializers

from ....payroll.models import CivilStatus


class CivilStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = CivilStatus
        fields = (
            'id',
            'name',
            'creation_date',
            'last_update'
        )
