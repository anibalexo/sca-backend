from drf_spectacular.utils import extend_schema_view, extend_schema
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from .serializers import CivilStatusSerializer
from ....payroll.models import CivilStatus


@extend_schema_view(
    retrieve=extend_schema(
        description='Detail civil status records'
    ),
    partial_update=extend_schema(
        description='Partial update civil status records'
    ),
    destroy=extend_schema(
        description='Delete civil status records'
    ),
    create=extend_schema(
        description='Create an civil status record'
    ),
    update=extend_schema(
        description='Update an civil status record'
    )
)
class CivilStatusViewSet(viewsets.ModelViewSet):
    queryset = CivilStatus.objects.all()
    authentication_classes = (JWTTokenUserAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = CivilStatusSerializer

    @extend_schema(
        description='List civil status records, paginated results with 100 records per page'
    )
    def list(self, request, *args, **kwargs):
        if bool(request.query_params):
            queryset = self.get_queryset()
            if "page" in request.query_params:
                queryset = self.paginate_queryset(queryset)
                serializer = CivilStatusSerializer(queryset, many=True, context={"request": request})
                return self.get_paginated_response(serializer.data)
