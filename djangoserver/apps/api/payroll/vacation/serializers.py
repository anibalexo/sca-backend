from rest_framework import serializers

from ....payroll.models import Vacation, Employee


class VacationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vacation
        fields = (
            'id',
            'paid',
            'erp_vacation_code',
            'employee',
            'exit_date',
            'entry_date',
            'observations',
            'creation_date',
            'last_update'
        )

class EmployeeNestedVacationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = (
            'id',
            'first_name',
            'last_name'
        )

class ListVacationSerializer(VacationSerializer):
    employee = EmployeeNestedVacationSerializer()
