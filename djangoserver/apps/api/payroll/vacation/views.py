from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from .serializers import VacationSerializer, ListVacationSerializer
from ....payroll.models import Vacation


@extend_schema_view(
    create=extend_schema(
        description='Create vacation records'
    ),
    update=extend_schema(
        description='Update vacation records'
    ),
    partial_update=extend_schema(
        description='Partial update vacation records'
    ),
    destroy=extend_schema(
        description='Delete vacation records'
    ),
)
class VacationViewSet(viewsets.ModelViewSet):
    queryset = Vacation.objects.all()
    authentication_classes = (JWTTokenUserAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = VacationSerializer

    @extend_schema(
        description='List vacation records, paginated results with 100 records per page',
        responses=ListVacationSerializer
    )
    def list(self, request, *args, **kwargs):
        if bool(request.query_params):
            queryset = self.get_queryset()
            if "page" in request.query_params:
                queryset = self.paginate_queryset(queryset)
                serializer = ListVacationSerializer(queryset, many=True, context={"request": request})
                return self.get_paginated_response(serializer.data)

    @extend_schema(
        description='Detail vacation record',
        responses=ListVacationSerializer
    )
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = ListVacationSerializer(instance=instance)
        return Response(serializer.data)
