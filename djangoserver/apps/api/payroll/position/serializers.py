from rest_framework import serializers

from ....payroll.models import Position


class PositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Position
        fields = (
            'id',
            'name',
            'erp_position_code',
            'creation_date',
            'last_update'
        )


class CreateUpdatePositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Position
        exclude = (
            'id',
            'creation_date',
            'last_update'
        )
