from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from .serializers import PositionSerializer
from ....payroll.models import Position


@extend_schema_view(
    retrieve=extend_schema(
        description='Detail position records'
    ),
    create=extend_schema(
        description='Create position records'
    ),
    update=extend_schema(
        description='Update position records'
    ),
    partial_update=extend_schema(
        description='Partial update position records'
    ),
    destroy=extend_schema(
        description='Delete position records'
    ),
)
class PositionViewSet(viewsets.ModelViewSet):
    queryset = Position.objects.all()
    authentication_classes = (JWTTokenUserAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = PositionSerializer

    @extend_schema(
        description='List position records, paginated results with 100 records per page'
    )
    def list(self, request, *args, **kwargs):
        if bool(request.query_params):
            queryset = self.get_queryset()
            if "page" in request.query_params:
                queryset = self.paginate_queryset(queryset)
                serializer = PositionSerializer(queryset, many=True, context={"request": request})
                return self.get_paginated_response(serializer.data)
