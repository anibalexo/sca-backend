from rest_framework import serializers

from ....payroll.models import Department


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = (
            'id',
            'name',
            'location',
            'erp_department_code',
            'creation_date',
            'last_update'
        )
