from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from .serializers import DepartmentSerializer
from ....payroll.models import Department


@extend_schema_view(
    retrieve=extend_schema(
        description='Detail department records'
    ),
    partial_update=extend_schema(
        description='Partial update department records'
    ),
    destroy=extend_schema(
        description='Delete department records'
    ),
    create=extend_schema(
        description='Create an department record'
    ),
    update=extend_schema(
        description='Update department record'
    )
)
class DepartmentViewSet(viewsets.ModelViewSet):
    queryset = Department.objects.all()
    authentication_classes = (JWTTokenUserAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = DepartmentSerializer

    @extend_schema(
        description='List department records, paginated results with 100 records per page'
    )
    def list(self, request, *args, **kwargs):
        if bool(request.query_params):
            queryset = self.get_queryset()
            if "page" in request.query_params:
                queryset = self.paginate_queryset(queryset)
                serializer = DepartmentSerializer(queryset, many=True, context={"request": request})
                return self.get_paginated_response(serializer.data)
