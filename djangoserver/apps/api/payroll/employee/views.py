from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from .serializers import EmployeeSerializer, ListEmployeeSerializer
from ....payroll.models import Employee


@extend_schema_view(
    create=extend_schema(
        description='Create employee records'
    ),
    update=extend_schema(
        description='Update employee records'
    ),
    partial_update=extend_schema(
        description='Partial update employee records'
    ),
    destroy=extend_schema(
        description='Delete employee records'
    ),
)
class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    authentication_classes = (JWTTokenUserAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = EmployeeSerializer

    @extend_schema(
        description='List employee records, paginated results with 100 records per page',
        responses=ListEmployeeSerializer
    )
    def list(self, request, *args, **kwargs):
        if bool(request.query_params):
            queryset = self.get_queryset()
            if "page" in request.query_params:
                queryset = self.paginate_queryset(queryset)
                serializer = ListEmployeeSerializer(queryset, many=True, context={"request": request})
                return self.get_paginated_response(serializer.data)

    @extend_schema(
        description='Detail employee record',
        responses=ListEmployeeSerializer
    )
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = ListEmployeeSerializer(instance=instance)
        return Response(serializer.data)
