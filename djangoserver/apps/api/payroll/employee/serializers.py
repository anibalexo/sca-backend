from rest_framework import serializers

from ....payroll.models import (
    Employee, CivilStatus, Position, Agency, Department
)
from ....devices.models import Device


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = (
            'id',
            'dni',
            'first_name',
            'last_name',
            'address',
            'phone',
            'mail',
            'residence',
            'birth_date',
            'active',
            'pay_extra',
            'pay_nightly_extra',
            'privilege',
            'device_employee_code',
            'erp_employee_code',
            'civil_status',
            'position',
            'agency',
            'department',
            'devices',
            'creation_date',
            'last_update'
        )

class CivilStatusNestedPermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = CivilStatus
        fields = (
            'id',
            'name'
        )

class PositionNestedPermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Position
        fields = (
            'id',
            'name',
            'erp_position_code'
        )

class AgencyNestedPermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Agency
        fields = (
            'id',
            'name',
            'erp_agency_code'
        )

class DepartmentNestedPermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = (
            'id',
            'name',
            'erp_department_code'
        )

class DeviceNestedPermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = (
            'id',
            'name',
            'location',
        )

class ListEmployeeSerializer(EmployeeSerializer):
    civil_status = CivilStatusNestedPermissionSerializer()
    position = PositionNestedPermissionSerializer()
    agency = AgencyNestedPermissionSerializer()
    department = DepartmentNestedPermissionSerializer()
    devices = DeviceNestedPermissionSerializer(many=True)

class CreateUpdateEmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        exclude = (
            'id',
            'creation_date',
            'last_update'
        )
