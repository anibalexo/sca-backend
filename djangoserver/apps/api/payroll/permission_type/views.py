from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from .serializers import PermissionTypeSerializer
from ....payroll.models import PermissionType


@extend_schema_view(
    retrieve=extend_schema(
        description='Detail permission type records'
    ),
    create=extend_schema(
        description='Create permission type records'
    ),
    update=extend_schema(
        description='Update permission type records'
    ),
    partial_update=extend_schema(
        description='Partial update permission type records'
    ),
    destroy=extend_schema(
        description='Delete permission type records'
    ),
)
class PermissionTypeViewSet(viewsets.ModelViewSet):
    queryset = PermissionType.objects.all()
    authentication_classes = (JWTTokenUserAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = PermissionTypeSerializer

    @extend_schema(
        description='List position records, paginated results with 100 records per page'
    )
    def list(self, request, *args, **kwargs):
        if bool(request.query_params):
            queryset = self.get_queryset()
            if "page" in request.query_params:
                queryset = self.paginate_queryset(queryset)
                serializer = PermissionTypeSerializer(queryset, many=True, context={"request": request})
                return self.get_paginated_response(serializer.data)
