from rest_framework import serializers

from ....payroll.models import PermissionType


class PermissionTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PermissionType
        fields = (
            'id',
            'name',
            'erp_permission_type_code',
            'creation_date',
            'last_update'
        )
