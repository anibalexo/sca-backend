from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from .serializers import AgencySerializer
from ....payroll.models import Agency


@extend_schema_view(
    retrieve=extend_schema(
        description='Detail agency records'
    ),
    partial_update=extend_schema(
        description='Partial update agency records'
    ),
    destroy=extend_schema(
        description='Delete agency records'
    ),
    create=extend_schema(
        description='Create an agency record'
    ),
    update=extend_schema(
        description='Update an agency record'
    )
)
class AgencyViewSet(viewsets.ModelViewSet):
    queryset = Agency.objects.all()
    authentication_classes = (JWTTokenUserAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = AgencySerializer

    @extend_schema(
        description='List agency records, paginated results with 100 records per page'
    )
    def list(self, request, *args, **kwargs):
        if bool(request.query_params):
            queryset = self.get_queryset()
            if "page" in request.query_params:
                queryset = self.paginate_queryset(queryset)
                serializer = AgencySerializer(queryset, many=True, context={"request": request})
                return self.get_paginated_response(serializer.data)
