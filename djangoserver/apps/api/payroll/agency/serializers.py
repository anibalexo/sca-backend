from rest_framework import serializers

from ....payroll.models import Agency


class AgencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Agency
        fields = (
            'id',
            'name',
            'description',
            'reference',
            'erp_agency_code',
            'creation_date',
            'last_update'
        )
