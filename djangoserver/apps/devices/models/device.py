from django.db import models


class Device(models.Model):
    name = models.CharField(max_length=80, unique=True, help_text='')
    location = models.CharField(max_length=150)
    ip_address = models.CharField(max_length=50, unique=True)
    port = models.PositiveSmallIntegerField(default=4370)
    active = models.BooleanField(default=True)
    code = models.CharField(max_length=20, unique=True)
    firmware = models.CharField(max_length=80, blank=True, null=True)
    name_device = models.CharField(max_length=80, blank=True, null=True)
    serial = models.CharField(max_length=80, blank=True, null=True)
    mac = models.CharField(max_length=80, blank=True, null=True)
    plataform = models.CharField(max_length=80, blank=True, null=True)
    number_records = models.IntegerField(default=0)
    capacity_records = models.IntegerField(default=0)
    agency = models.ForeignKey('payroll.Agency', on_delete=models.CASCADE, related_name='devices_agency')
    creation_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-name']

    def __str__(self):
        return "{} - {}".format(self.name, self.ip_address)
