from django.db import models


class Attendance(models.Model):
    date = models.DateTimeField(help_text='Date employee recorded attendance')
    type = models.CharField(max_length=1, default='I')
    employee = models.ForeignKey('payroll.Employee', on_delete=models.CASCADE, related_name='attendances_employee',
                                 help_text='Employee id')
    device = models.ForeignKey('Device', on_delete=models.CASCADE, related_name='attendances_device',
                               help_text='Device id')
    creation_date = models.DateTimeField(auto_now_add=True, help_text='Date the record was created')
    last_update = models.DateTimeField(auto_now=True, help_text='Date of last update of the registry')

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return "{} - {}".format(self.employee, self.date)

