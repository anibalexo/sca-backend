from django.db import models


class Record(models.Model):
    uid = models.IntegerField(help_text='User ID that are generated from device')
    user_id = models.CharField(max_length=10,help_text='Your own user ID')
    date = models.DateTimeField(help_text='Employee recorded attendance date on device')
    status = models.IntegerField(help_text='Check status')
    punch = models.IntegerField()
    used = models.BooleanField(help_text='Record has been used in Attendance model', default=False)
    device = models.ForeignKey('Device', on_delete=models.CASCADE, related_name='records_device',
                               help_text='Device id')
    creation_date = models.DateTimeField(auto_now_add=True, help_text='Date the record was created')
    last_update = models.DateTimeField(auto_now=True, help_text='Date of last update of the registry')

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return "{} - {}".format(self.user_id, self.date)
