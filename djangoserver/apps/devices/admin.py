from django.contrib import admin

from .models import Device, Attendance, Record

class DeviceAdmin(admin.ModelAdmin):
    list_display = ('name', 'location', 'ip_address', 'active', 'agency')
    search_fields = ('name', 'location', 'agency__name')
    list_filter = ('active', 'agency')
    list_per_page = 20
    readonly_fields = ('creation_date', 'last_update')

class AttendanceAdmin(admin.ModelAdmin):
    list_display = ('pk', 'date', 'employee', 'device', 'creation_date')
    search_fields = ('employee__name', 'device__name')
    list_filter = ('device', 'employee')
    list_per_page = 20
    readonly_fields = ('creation_date', 'last_update')

class RecordAdmin(admin.ModelAdmin):
    list_display = ('pk', 'user_id', 'date', 'used', 'device')
    search_fields = ('user_id', 'device__name')
    list_filter = ('used', 'device')
    list_per_page = 20
    readonly_fields = ('creation_date', 'last_update')

admin.site.register(Device, DeviceAdmin)
admin.site.register(Attendance, AttendanceAdmin)
admin.site.register(Record, RecordAdmin)
