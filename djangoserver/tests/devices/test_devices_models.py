import pytest
from apps.devices.models import Device, Attendance, Record
from model_bakery import baker
from rest_framework.test import APIClient


ApiClient = APIClient()

class TestDevicesModels:

    @pytest.mark.django_db
    def test_attendance_model(self):
        attendance = baker.make(Attendance)
        assert attendance.__str__() == "{} - {}".format(attendance.employee, attendance.date)

    @pytest.mark.django_db
    def test_device_model(self):
        device = baker.make(Device)
        assert device.__str__() == "{} - {}".format(device.name, device.ip_address)

    @pytest.mark.django_db
    def test_record_model(self):
        record = baker.make(Record)
        assert record.__str__() == "{} - {}".format(record.user_id, record.date)
