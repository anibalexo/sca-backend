import pytest
from django.contrib.auth import get_user_model
from django.forms import model_to_dict
from django.urls import reverse_lazy
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import AccessToken

ApiClient = APIClient()

class TestDeviceAPI:

    endpoint_url = reverse_lazy('api:device-list')

    @pytest.fixture()
    def setup(self):
        User = get_user_model()
        user_test = User.objects.create_user(email='example@gmail.com', password='1234')
        ApiClient.credentials(
            HTTP_AUTHORIZATION="Bearer {}".format(AccessToken.for_user(user_test))
        )

    @pytest.mark.django_db
    def test_api_device_list(self, setup):
        baker.make('devices.Device', _quantity=3)
        response = ApiClient.get(f"{self.endpoint_url}?page=1", format='json')
        assert len(response.data['results']) == 3
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_device_create(self, setup):
        agency = baker.make('payroll.Agency')
        expected_json = {
            "name": "example name",
            "location": "example location",
            "ip_address": "127.0.0.1",
            "port": 4370,
            "active": True,
            "code": "001",
            "firmware": "example firmware",
            "name_device": "example name device",
            "serial": "example serial",
            "mac": "example mac",
            "plataform": "example plataform",
            "number_records": 0,
            "capacity_records": 0,
            "agency": agency.pk
        }
        response = ApiClient.post(self.endpoint_url, expected_json, format='json')
        assert response.data["name"] == expected_json["name"]
        assert response.data["location"] == expected_json["location"]
        assert response.status_code == status.HTTP_201_CREATED

    @pytest.mark.django_db
    def test_api_device_retrieve(self, setup):
        device = baker.make(
            'devices.Device',
            name="Test name device",
            firmware="test firmware",
            name_device="test name device",
            serial="test serial",
            mac="test mac",
            plataform="test plataform"
        )
        expected_json = model_to_dict(device)
        expected_json["creation_date"] = device.creation_date.strftime("%Y-%m-%dT%H:%M:%S.%f")
        expected_json["last_update"] = device.last_update.strftime("%Y-%m-%dT%H:%M:%S.%f")
        response = ApiClient.get(f"{self.endpoint_url}{device.pk}/", format='json')
        assert response.data == expected_json
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_vacation_update(self, setup):
        device = baker.make(
            'devices.Device',
            name="Test name device",
            firmware="test firmware",
            name_device="test name device",
            serial="test serial",
            mac="test mac",
            plataform="test plataform"
        )
        expected_json = model_to_dict(device)
        expected_json["name"] = "Test name device update"
        expected_json["location"] = "Test location device update"
        response = ApiClient.put(f"{self.endpoint_url}{device.pk}/", expected_json, format='json')
        assert response.data["name"] == expected_json["name"]
        assert response.data["location"] == expected_json["location"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_vacation_partial_update(self, setup):
        device = baker.make(
            'devices.Device',
            name="Test name device",
            firmware="test firmware",
            name_device="test name device",
            serial="test serial",
            mac="test mac",
            plataform="test plataform"
        )
        expected_json = {
            "name": "Test name partial update"
        }
        response = ApiClient.patch(f"{self.endpoint_url}{device.pk}/", expected_json, format='json')
        assert response.data["name"] == expected_json["name"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_device_delete(self, setup):
        device = baker.make('devices.Device')
        response = ApiClient.delete(f"{self.endpoint_url}{device.pk}/", format='json')
        assert response.status_code == status.HTTP_204_NO_CONTENT
