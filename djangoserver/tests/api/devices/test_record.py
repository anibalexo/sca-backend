import json

import pytest
from django.contrib.auth import get_user_model
from django.urls import reverse_lazy
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import AccessToken

ApiClient = APIClient()

class TestRecordAPI:
    endpoint_url = reverse_lazy('record')

    @pytest.fixture()
    def setup(self):
        User = get_user_model()
        user_test = User.objects.create_user(email='example@gmail.com', password='1234')
        ApiClient.credentials(
            HTTP_AUTHORIZATION="Bearer {}".format(AccessToken.for_user(user_test))
        )

    @pytest.mark.django_db
    def test_api_record_list(self, setup):
        baker.make('devices.Record', _quantity=10)
        url_list = f"{self.endpoint_url}?page=1"
        response = ApiClient.get(url_list, format='json')
        assert len(response.data['results']) == 10
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_record_create(self, setup):
        device = baker.make('devices.Device')
        TEST_SIZE = 10
        data = [
            {"uid": int(x), "user_id":"11105478", "date":"2022-07-29T10:38:02",
             "status":1, "punch":1, "device":device.pk} for x in range(TEST_SIZE)
        ]
        response = ApiClient.post(self.endpoint_url, data=json.dumps(data), content_type="application/json")
        assert response.status_code == status.HTTP_201_CREATED
        assert len(response.data) == TEST_SIZE
