import pytest
from django.contrib.auth import get_user_model
from django.forms import model_to_dict
from django.urls import reverse_lazy
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import AccessToken

ApiClient = APIClient()

class TestAttendanceAPI:

    endpoint_url = reverse_lazy('api:attendance-list')

    @pytest.fixture()
    def setup(self):
        User = get_user_model()
        user_test = User.objects.create_user(email='example@gmail.com', password='1234')
        ApiClient.credentials(
            HTTP_AUTHORIZATION="Bearer {}".format(AccessToken.for_user(user_test))
        )

    @pytest.mark.django_db
    def test_api_attendance_list(self, setup):
        baker.make('devices.Attendance', _quantity=3)
        response = ApiClient.get(f"{self.endpoint_url}?page=1", format='json')
        assert len(response.data['results']) == 3
        assert response.status_code == status.HTTP_200_OK

    # def test_api_attendance_list_without_jwt(self):
    #     ApiClient.credentials(HTTP_AUTHORIZATION=None)
    #     url_list = f"{self.endpoint_url}?page=1"
    #     response = ApiClient.get(url_list, format='json')
    #     assert 'detail' in response.data
    #     assert response.status_code == status.HTTP_401_UNAUTHORIZED

    @pytest.mark.django_db
    def test_api_attendance_create(self, setup):
        employee = baker.make('payroll.Employee')
        device = baker.make('devices.Device')
        attendance = baker.prepare('devices.Attendance')
        expected_json = {
            "date": attendance.date.strftime("%Y-%m-%dT%H:%M:%S"),
            "employee": employee.pk,
            "device": device.pk
        }
        response = ApiClient.post(self.endpoint_url, expected_json, format='json')
        assert response.data["date"] == expected_json["date"]
        assert response.data["employee"] == expected_json["employee"]
        assert response.data["device"] == expected_json["device"]
        assert response.status_code == status.HTTP_201_CREATED

    @pytest.mark.django_db
    def test_api_attendance_retrieve(self, setup):
        attendance = baker.make('devices.Attendance')
        expected_json = model_to_dict(attendance)
        expected_json["date"] = attendance.date.strftime("%Y-%m-%dT%H:%M:%S.%f")
        expected_json["creation_date"] = attendance.creation_date.strftime("%Y-%m-%dT%H:%M:%S.%f")
        expected_json["last_update"] = attendance.last_update.strftime("%Y-%m-%dT%H:%M:%S.%f")
        response = ApiClient.get(f"{self.endpoint_url}{attendance.pk}/", format='json')
        assert response.data == expected_json
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_attendance_update(self, setup):
        attendance = baker.make('devices.Attendance')
        expected_json = {
            "date": "2022-01-01T12:00:00",
            "employee": attendance.employee.pk,
            "device": attendance.device.pk
        }
        url_update = f"{self.endpoint_url}{attendance.pk}/"
        response = ApiClient.put(url_update, expected_json, format='json')
        assert response.data['date'] == expected_json['date']
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_attendance_partial_update(self, setup):
        attendance = baker.make('devices.Attendance')
        expected_json = {
            "date": "2022-12-31T18:00:00",
        }
        response = ApiClient.patch(f"{self.endpoint_url}{attendance.pk}/", expected_json, format='json')
        assert response.data["date"] == expected_json["date"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_attendance_delete(self, setup):
        attendance = baker.make('devices.Attendance')
        response = ApiClient.delete(f"{self.endpoint_url}{attendance.pk}/", format='json')
        assert response.status_code == status.HTTP_204_NO_CONTENT
