import pytest
from django.contrib.auth import get_user_model
from django.forms import model_to_dict
from django.urls import reverse_lazy
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import AccessToken

ApiClient = APIClient()


class TestDepartmentAPI:
    endpoint_url = reverse_lazy('api:department-list')

    @pytest.fixture()
    def setup(self):
        User = get_user_model()
        user_test = User.objects.create_user(email='example@gmail.com', password='1234')
        ApiClient.credentials(
            HTTP_AUTHORIZATION="Bearer {}".format(AccessToken.for_user(user_test))
        )

    @pytest.mark.django_db
    def test_api_department_list(self, setup):
        baker.make('payroll.Department', _quantity=3)
        response = ApiClient.get(f"{self.endpoint_url}?page=1", format='json')
        assert len(response.data['results']) == 3
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_department_create(self, setup):
        expected_json = {
            "name": "Test name",
            "location": "Test location",
            "erp_department_code": "DPT0001"
        }
        response = ApiClient.post(self.endpoint_url, expected_json, format='json')
        assert response.data["name"] == expected_json["name"]
        assert response.data["location"] == expected_json["location"]
        assert response.data["erp_department_code"] == expected_json["erp_department_code"]
        assert response.status_code == status.HTTP_201_CREATED

    @pytest.mark.django_db
    def test_api_department_retrieve(self, setup):
        department = baker.make('payroll.Department')
        expected_json = model_to_dict(department)
        expected_json["creation_date"] = department.creation_date.strftime("%Y-%m-%dT%H:%M:%S.%f")
        expected_json["last_update"] = department.last_update.strftime("%Y-%m-%dT%H:%M:%S.%f")
        response = ApiClient.get(f"{self.endpoint_url}{department.pk}/", format='json')
        assert response.data == expected_json
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_department_update(self, setup):
        department = baker.make('payroll.Department')
        expected_json = {
            "name": "Test name department",
            "location": department.location,
            "erp_department_code": department.erp_department_code
        }
        response = ApiClient.put(f"{self.endpoint_url}{department.pk}/", expected_json, format='json')
        assert response.data["name"] == expected_json["name"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_department_partial_update(self, setup):
        department = baker.make('payroll.Department')
        expected_json = {
            "name": "Test partial update name",
        }
        response = ApiClient.patch(f"{self.endpoint_url}{department.pk}/", expected_json, format='json')
        assert response.data["name"] == expected_json["name"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_department_delete(self, setup):
        department = baker.make('payroll.Department')
        response = ApiClient.delete(f"{self.endpoint_url}{department.pk}/", format='json')
        assert response.status_code == status.HTTP_204_NO_CONTENT
