import pytest
from django.contrib.auth import get_user_model
from django.forms import model_to_dict
from django.urls import reverse_lazy
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import AccessToken

ApiClient = APIClient()


class TestPermissionAPI:
    endpoint_url = reverse_lazy('api:permission-list')

    @pytest.fixture()
    def setup(self):
        User = get_user_model()
        user_test = User.objects.create_user(email='example@gmail.com', password='1234')
        ApiClient.credentials(
            HTTP_AUTHORIZATION="Bearer {}".format(AccessToken.for_user(user_test))
        )

    @pytest.mark.django_db
    def test_api_permission_list(self, setup):
        baker.make('payroll.Permission', _quantity=3)
        response = ApiClient.get(f"{self.endpoint_url}?page=1", format='json')
        assert len(response.data['results']) == 3
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_permission_create(self, setup):
        permission_type = baker.make('payroll.PermissionType')
        employee = baker.make('payroll.Employee')
        expected_json = {
            "application_date": "2022-10-01T16:37:13",
            "reason": "Test create permision reason",
            "measure_time": "D",
            "estimated_time": "01:00:00",
            "justified": True,
            "erp_permission_code": "PRM0002",
            "exit_time": "11:37:23",
            "entry_time": "11:37:24",
            "permission_type": permission_type.pk,
            "employee": employee.pk,
            "exit_date": "2022-10-01",
            "entry_date": "2022-10-01",
            "observations": "test observaciones"
        }
        response = ApiClient.post(self.endpoint_url, expected_json, format='json')
        assert response.data["application_date"] == expected_json["application_date"]
        assert response.data["reason"] == expected_json["reason"]
        assert response.data["erp_permission_code"] == expected_json["erp_permission_code"]
        assert response.status_code == status.HTTP_201_CREATED

    @pytest.mark.django_db
    def test_api_permission_retrieve(self, setup):
        permission = baker.make(
            'payroll.Permission',
            estimated_time='01:00:00',
            exit_time='11:37:23',
            entry_time='17:00:00',
            erp_permission_code='PR0001',
            exit_date='2022-01-01',
            entry_date='2022-01-01',
            observations='Example Observations'
        )
        expected_json = model_to_dict(permission)
        expected_json["application_date"] = permission.application_date.strftime("%Y-%m-%dT%H:%M:%S.%f")
        expected_json["permission_type"] = model_to_dict(permission.permission_type,
                                                         fields=['id', 'name','erp_permission_type_code'])
        expected_json["employee"] = model_to_dict(permission.employee, fields=['id', 'first_name', 'last_name'])
        expected_json["creation_date"] = permission.creation_date.strftime("%Y-%m-%dT%H:%M:%S.%f")
        expected_json["last_update"] = permission.last_update.strftime("%Y-%m-%dT%H:%M:%S.%f")
        response = ApiClient.get(f"{self.endpoint_url}{permission.pk}/", expected_json, format='json')
        assert response.data == expected_json
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_permission_update(self, setup):
        permission = baker.make('payroll.Permission')
        expected_json = {
            "application_date": permission.application_date.strftime("%Y-%m-%dT%H:%M:%S"),
            "reason": "Asuntos personales",
            "measure_time": permission.measure_time,
            "estimated_time": permission.estimated_time,
            "justified": permission.justified,
            "erp_permission_code": permission.erp_permission_code,
            "exit_time": permission.exit_time,
            "entry_time": permission.entry_time,
            "permission_type": permission.permission_type.pk,
            "employee": permission.employee.pk,
            "exit_date": permission.exit_date,
            "entry_date": permission.entry_date,
            "observations": permission.observations
        }
        response = ApiClient.put(f"{self.endpoint_url}{permission.pk}/", expected_json, format='json')
        assert response.data["reason"] == expected_json["reason"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_permission_partial_update(self, setup):
        permission = baker.make('payroll.Permission')
        datajson = {
            "reason": "Test partial update reason",
        }
        response = ApiClient.patch(f"{self.endpoint_url}{permission.pk}/", datajson, format='json')
        assert response.data["reason"] == datajson["reason"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_permission_delete(self, setup):
        permission = baker.make('payroll.Permission')
        response = ApiClient.delete(f"{self.endpoint_url}{permission.pk}/", format='json')
        assert response.status_code == status.HTTP_204_NO_CONTENT
