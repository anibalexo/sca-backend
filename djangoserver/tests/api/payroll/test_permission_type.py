import pytest
from django.contrib.auth import get_user_model
from django.forms import model_to_dict
from django.urls import reverse_lazy
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import AccessToken

ApiClient = APIClient()


class TestPermissionTypeAPI:
    endpoint_url = reverse_lazy('api:permission-type-list')

    @pytest.fixture()
    def setup(self):
        User = get_user_model()
        user_test = User.objects.create_user(email='example@gmail.com', password='1234')
        ApiClient.credentials(
            HTTP_AUTHORIZATION="Bearer {}".format(AccessToken.for_user(user_test))
        )

    @pytest.mark.django_db
    def test_api_permissiontype_list(self, setup):
        baker.make('payroll.PermissionType', _quantity=3)
        response = ApiClient.get(f"{self.endpoint_url}?page=1", format='json')
        assert len(response.data['results']) == 3
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_permissiontype_create(self, setup):
        expected_json = {
            "name": "Test create permission type",
            "erp_permission_type_code": "TP0000",
        }
        response = ApiClient.post(self.endpoint_url, expected_json, format='json')
        assert response.data["name"] == expected_json["name"]
        assert response.data["erp_permission_type_code"] == expected_json["erp_permission_type_code"]
        assert response.status_code == status.HTTP_201_CREATED

    @pytest.mark.django_db
    def test_api_permissiontype_retrieve(self, setup):
        permission_type = baker.make('payroll.PermissionType')
        expected_json = model_to_dict(permission_type)
        expected_json["creation_date"] = permission_type.creation_date.strftime("%Y-%m-%dT%H:%M:%S.%f")
        expected_json["last_update"] = permission_type.last_update.strftime("%Y-%m-%dT%H:%M:%S.%f")
        response = ApiClient.get(f"{self.endpoint_url}{permission_type.pk}/", format='json')
        assert response.data == expected_json
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_permissiontype_update(self, setup):
        permissiontype = baker.make('payroll.PermissionType')
        expected_json = {
            "name": "Test update permission type",
            "erp_permission_type_code": permissiontype.erp_permission_type_code
        }
        response = ApiClient.put(f"{self.endpoint_url}{permissiontype.pk}/", expected_json, format='json')
        assert response.data["name"] == expected_json["name"]
        assert response.data["erp_permission_type_code"] == expected_json["erp_permission_type_code"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_permissiontype_partial_update(self, setup):
        permissiontype = baker.make('payroll.PermissionType')
        datajson = {
            "name": "Test partial update permission type",
        }
        response = ApiClient.patch(f"{self.endpoint_url}{permissiontype.pk}/", datajson, format='json')
        assert response.data["name"] == datajson["name"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_permissiontype_delete(self, setup):
        permissiontype = baker.make('payroll.PermissionType')
        response = ApiClient.delete(f"{self.endpoint_url}{permissiontype.pk}/", format='json')
        assert response.status_code == status.HTTP_204_NO_CONTENT
