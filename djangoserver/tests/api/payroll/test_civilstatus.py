import pytest
from django.contrib.auth import get_user_model
from django.forms import model_to_dict
from django.urls import reverse_lazy
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import AccessToken

ApiClient = APIClient()


class TestCivilStatusAPI:
    endpoint_url = reverse_lazy('api:civil-status-list')

    @pytest.fixture()
    def setup(self):
        User = get_user_model()
        user_test = User.objects.create_user(email='example@gmail.com', password='1234')
        ApiClient.credentials(
            HTTP_AUTHORIZATION="Bearer {}".format(AccessToken.for_user(user_test))
        )

    @pytest.mark.django_db
    def test_api_civilstatus_list(self, setup):
        baker.make('payroll.CivilStatus', _quantity=3)
        url_list = f"{self.endpoint_url}?page=1"
        response = ApiClient.get(url_list, format='json')
        assert len(response.data['results']) == 3
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_civilstatus_create(self, setup):
        expected_json = {
            "name": "test name civil status"
        }
        response = ApiClient.post(self.endpoint_url, expected_json, format='json')
        assert response.data["name"] == expected_json["name"]
        assert response.status_code == status.HTTP_201_CREATED

    @pytest.mark.django_db
    def test_api_civilstatus_retrieve(self, setup):
        civilstatus = baker.make('payroll.CivilStatus')
        expected_json = model_to_dict(civilstatus)
        expected_json["creation_date"] = civilstatus.creation_date.strftime("%Y-%m-%dT%H:%M:%S.%f")
        expected_json["last_update"] = civilstatus.last_update.strftime("%Y-%m-%dT%H:%M:%S.%f")
        response = ApiClient.get(f"{self.endpoint_url}{civilstatus.pk}/", format='json')
        assert response.data == expected_json
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_civilstatus_update(self, setup):
        civilstatus = baker.make('payroll.CivilStatus')
        expected_json = {
            "name": "Test Update Name"
        }
        url_update = f"{self.endpoint_url}{civilstatus.pk}/"
        response = ApiClient.put(url_update, expected_json, format='json')
        assert response.data['name'] == expected_json['name']
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_civilstatus_partial_update(self, setup):
        civilstatus = baker.make('payroll.CivilStatus')
        expected_json = {
            "name": "Test partial update name",
        }
        response = ApiClient.patch(f"{self.endpoint_url}{civilstatus.pk}/", expected_json, format='json')
        assert response.data["name"] == expected_json["name"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_civilstatus_delete(self, setup):
        civilstatus = baker.make('payroll.CivilStatus')
        response = ApiClient.delete(f"{self.endpoint_url}{civilstatus.pk}/", format='json')
        assert response.status_code == status.HTTP_204_NO_CONTENT
