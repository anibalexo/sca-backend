import pytest
from django.contrib.auth import get_user_model
from django.forms.models import model_to_dict
from django.urls import reverse_lazy
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import AccessToken

ApiClient = APIClient()


class TestEmployeeAPI:
    endpoint_url = reverse_lazy('api:employee-list')

    @pytest.fixture()
    def setup(self):
        User = get_user_model()
        user_test = User.objects.create_user(email='example@gmail.com', password='1234')
        ApiClient.credentials(
            HTTP_AUTHORIZATION="Bearer {}".format(AccessToken.for_user(user_test))
        )

    @pytest.mark.django_db
    def test_api_employee_list(self, setup):
        baker.make('payroll.Employee', _quantity=3)
        response = ApiClient.get(f"{self.endpoint_url}?page=1", format='json')
        assert len(response.data['results']) == 3
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_employee_create(self, setup):
        civil_status = baker.make('payroll.CivilStatus')
        position = baker.make('payroll.Position')
        agency = baker.make('payroll.Agency')
        department = baker.make('payroll.Department')
        devices = baker.make('devices.Device', _quantity=2)
        employee_json = {
            "dni": "9999999999",
            "first_name": "Name Test",
            "last_name": "Last name test",
            "address": "address test",
            "phone": "0989999999",
            "mail": "test@gmail.com",
            "residence": "test city",
            "birth_date": "2022-01-01",
            "active": True,
            "pay_extra": True,
            "pay_nightly_extra": True,
            "privilege": "I",
            "device_employee_code": 1,
            "erp_employee_code": "EMP00002",
            "civil_status": civil_status.pk,
            "position": position.pk,
            "agency": agency.pk,
            "department": department.pk,
            "devices": [a.pk for a in devices]
        }
        response = ApiClient.post(self.endpoint_url, employee_json, format='json')
        assert response.status_code == status.HTTP_201_CREATED

    @pytest.mark.django_db
    def test_api_employee_retrieve(self, setup):
        employee = baker.make(
            'payroll.Employee',
            phone='999999999',
            mail='test@mail.com',
            residence='Test residence atribute',
            privilege='I',
            erp_employee_code='EMP0000'
        )

        expected_json = model_to_dict(employee)
        expected_json["birth_date"] = employee.birth_date.strftime("%Y-%m-%d")
        expected_json["civil_status"] = model_to_dict(employee.civil_status, fields=['id', 'name'])
        expected_json["position"] = model_to_dict(employee.position, fields=['id', 'name', 'erp_position_code'])
        expected_json["agency"] = model_to_dict(employee.agency, fields=['id', 'name', 'erp_agency_code'])
        expected_json["department"] = model_to_dict(employee.department, fields=['id', 'name', 'erp_department_code'])
        expected_json["devices"] = list(employee.devices.all().values('id', 'name', 'location'))
        expected_json["creation_date"] = employee.creation_date.strftime("%Y-%m-%dT%H:%M:%S.%f")
        expected_json["last_update"] = employee.last_update.strftime("%Y-%m-%dT%H:%M:%S.%f")
        response = ApiClient.get(f"{self.endpoint_url}{employee.pk}/", expected_json, format='json')
        assert response.data == expected_json
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_employee_update(self, setup):
        employee = baker.make(
            'payroll.Employee',
            phone='999999999',
            mail='test@mail.com',
            residence='Test residence atribute',
            privilege='I',
            erp_employee_code='EMP0000'
        )
        expected_json = {
            "dni": employee.dni,
            "first_name": "Test change first name",
            "last_name": "Test change last name",
            "address": employee.address,
            "phone": employee.phone,
            "mail": employee.mail,
            "residence": employee.residence,
            "birth_date": employee.birth_date.strftime("%Y-%m-%d"),
            "active": employee.active,
            "pay_extra": employee.pay_extra,
            "pay_nightly_extra": employee.pay_nightly_extra,
            "privilege": employee.privilege,
            "device_employee_code": employee.device_employee_code,
            "erp_employee_code": employee.erp_employee_code,
            "civil_status": employee.civil_status.pk,
            "position": employee.position.pk,
            "agency": employee.agency.pk,
            "department": employee.department.pk
        }
        response = ApiClient.put(f"{self.endpoint_url}{employee.pk}/", expected_json, format='json')
        assert response.data["first_name"] == expected_json["first_name"]
        assert response.data["last_name"] == expected_json["last_name"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_employee_partial_update(self, setup):
        employee = baker.make(
            'payroll.Employee',
            phone='999999999',
            mail='test@mail.com',
            residence='Test residence atribute',
            privilege='I',
            erp_employee_code='EMP0000'
        )

        expected_json = {
            "first_name": "Test partial update first name",
            "last_name": "Test partial update last name",
            "address": "Test partial update address"
        }
        response = ApiClient.patch(f"{self.endpoint_url}{employee.pk}/", expected_json, format='json')
        assert response.data["first_name"] == expected_json["first_name"]
        assert response.data["last_name"] == expected_json["last_name"]
        assert response.data["address"] == expected_json["address"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_employee_delete(self, setup):
        employee = baker.make(
            'payroll.Employee',
            phone='999999999',
            mail='test@mail.com',
            residence='Test residence atribute',
            privilege='I',
            erp_employee_code='EMP0000'
        )
        response = ApiClient.delete(f"{self.endpoint_url}{employee.pk}/", format='json')
        assert response.status_code == status.HTTP_204_NO_CONTENT
