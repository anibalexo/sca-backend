import pytest
from django.contrib.auth import get_user_model
from django.forms import model_to_dict
from django.urls import reverse_lazy
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import AccessToken

ApiClient = APIClient()


class TestPositionAPI:
    endpoint_url = reverse_lazy('api:position-list')

    @pytest.fixture()
    def setup(self):
        User = get_user_model()
        user_test = User.objects.create_user(email='example@gmail.com', password='1234')
        ApiClient.credentials(
            HTTP_AUTHORIZATION="Bearer {}".format(AccessToken.for_user(user_test))
        )

    @pytest.mark.django_db
    def test_api_position_list(self, setup):
        baker.make('payroll.Position', _quantity=3)
        response = ApiClient.get(f"{self.endpoint_url}?page=1", format='json')
        assert len(response.data['results']) == 3
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_position_create(self, setup):
        expected_json = {
            "name": "Test create position",
            "erp_position_code": "P0001"
        }
        response = ApiClient.post(self.endpoint_url, expected_json, format='json')
        assert response.data["name"] == expected_json["name"]
        assert response.data["erp_position_code"] == expected_json["erp_position_code"]
        assert response.status_code == status.HTTP_201_CREATED

    @pytest.mark.django_db
    def test_api_position_retrieve(self, setup):
        position = baker.make('payroll.Position')
        expected_json = model_to_dict(position)
        expected_json["creation_date"] = position.creation_date.strftime("%Y-%m-%dT%H:%M:%S.%f")
        expected_json["last_update"] = position.last_update.strftime("%Y-%m-%dT%H:%M:%S.%f")
        response = ApiClient.get(f"{self.endpoint_url}{position.pk}/", format='json')
        assert response.data == expected_json
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_position_update(self, setup):
        position = baker.make('payroll.Position')
        expected_json = {
            "name": "Test update position",
            "erp_position_code": position.erp_position_code
        }
        response = ApiClient.put(f"{self.endpoint_url}{position.pk}/", expected_json, format='json')
        assert response.data["name"] == expected_json["name"]
        assert response.data["erp_position_code"] == expected_json["erp_position_code"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_position_partial_update(self, setup):
        position = baker.make('payroll.Position')
        datajson = {
            "name": "Test partial update position",
        }
        response = ApiClient.patch(f"{self.endpoint_url}{position.pk}/", datajson, format='json')
        assert response.data["name"] == datajson["name"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_position_delete(self, setup):
        position = baker.make('payroll.Position')
        response = ApiClient.delete(f"{self.endpoint_url}{position.pk}/", format='json')
        assert response.status_code == status.HTTP_204_NO_CONTENT
