import pytest
from django.contrib.auth import get_user_model
from django.forms import model_to_dict
from django.urls import reverse_lazy
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import AccessToken

ApiClient = APIClient()


class TestVacationAPI:
    endpoint_url = reverse_lazy('api:vacation-list')

    @pytest.fixture()
    def setup(self):
        User = get_user_model()
        user_test = User.objects.create_user(email='example@gmail.com', password='1234')
        ApiClient.credentials(
            HTTP_AUTHORIZATION="Bearer {}".format(AccessToken.for_user(user_test))
        )

    @pytest.mark.django_db
    def test_api_vacation_list(self, setup):
        baker.make('payroll.Vacation', _quantity=3)
        response = ApiClient.get(f"{self.endpoint_url}?page=1", format='json')
        assert len(response.data['results']) == 3
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_vacation_create(self, setup):
        employee = baker.make('payroll.Employee')
        expected_json = {
            "paid": True,
            "erp_vacation_code": "VCT0000",
            "employee": employee.pk,
            "exit_date": "2022-10-22",
            "entry_date": "2022-10-23",
            "observations": "test de observations"
        }
        response = ApiClient.post(self.endpoint_url, expected_json, format='json')
        assert response.data["erp_vacation_code"] == expected_json["erp_vacation_code"]
        assert response.data["employee"] == expected_json["employee"]
        assert response.data["exit_date"] == expected_json["exit_date"]
        assert response.data["entry_date"] == expected_json["entry_date"]
        assert response.status_code == status.HTTP_201_CREATED

    @pytest.mark.django_db
    def test_api_vacation_retrieve(self, setup):
        vacation = baker.make(
            'payroll.Vacation',
            erp_vacation_code='VCT0001',
            exit_date='2022-01-01',
            entry_date='2022-01-01',
            observations='Example Observations'
        )
        expected_json = model_to_dict(vacation)
        expected_json["employee"] = model_to_dict(vacation.employee, fields=['id','first_name','last_name'])
        expected_json["creation_date"] = vacation.creation_date.strftime("%Y-%m-%dT%H:%M:%S.%f")
        expected_json["last_update"] = vacation.last_update.strftime("%Y-%m-%dT%H:%M:%S.%f")
        response = ApiClient.get(f"{self.endpoint_url}{vacation.pk}/", format='json')
        assert response.data == expected_json
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_vacation_update(self, setup):
        vacation = baker.make(
            'payroll.Vacation',
            erp_vacation_code='VCT0001',
            exit_date='2022-01-01',
            entry_date='2022-01-01',
            observations='Example Observations'
        )
        expected_json = model_to_dict(vacation)
        expected_json["exit_date"] = "2022-01-03"
        expected_json["entry_date"] = "2022-01-04"
        response = ApiClient.put(f"{self.endpoint_url}{vacation.pk}/", expected_json, format='json')
        assert response.data["exit_date"] == expected_json["exit_date"]
        assert response.data["entry_date"] == expected_json["entry_date"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_vacation_partial_update(self, setup):
        vacation = baker.make(
            'payroll.Vacation',
            erp_vacation_code='VCT0001',
            exit_date='2022-01-01',
            entry_date='2022-01-01',
            observations='Example Observations'
        )
        expected_json = {
            "observations": "Test observations partial update"
        }
        response = ApiClient.patch(f"{self.endpoint_url}{vacation.pk}/", expected_json, format='json')
        assert response.data["observations"] == expected_json["observations"]
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_api_vacation_delete(self, setup):
        vacation = baker.make(
            'payroll.Vacation',
            erp_vacation_code='VCT0001',
            exit_date='2022-01-01',
            entry_date='2022-01-01',
            observations='Example Observations'
        )
        response = ApiClient.delete(f"{self.endpoint_url}{vacation.pk}/", format='json')
        assert response.status_code == status.HTTP_204_NO_CONTENT
