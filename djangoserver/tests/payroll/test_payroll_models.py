import pytest
from apps.payroll.models import (
    Agency, CivilStatus, Department, Employee, Permission,
    PermissionType, Position, Vacation
)
from model_bakery import baker
from rest_framework.test import APIClient


ApiClient = APIClient()

class TestPayrollModels:

    @pytest.mark.django_db
    def test_agency_model(self):
        agency = baker.make(Agency)
        assert agency.__str__() == "{}".format(agency.name)

    @pytest.mark.django_db
    def test_civilstatus_model(self):
        civil_status = baker.make(CivilStatus)
        assert civil_status.__str__() == "{}".format(civil_status.name)

    @pytest.mark.django_db
    def test_department_model(self):
        department = baker.make(Department)
        assert department.__str__() == "{} - {}".format(department.name, department.location)

    @pytest.mark.django_db
    def test_employee_model(self):
        employee = baker.make(Employee)
        assert employee.__str__() == "{} {}".format(employee.first_name, employee.last_name)
        assert employee.full_name == "{} {}".format(employee.first_name, employee.last_name)

    @pytest.mark.django_db
    def test_permission_model(self):
        permission = baker.make(Permission)
        assert permission.__str__() == "{} - {}".format(permission.reason, permission.employee)

    @pytest.mark.django_db
    def test_permissionType_model(self):
        permissionType = baker.make(PermissionType)
        assert permissionType.__str__() == "{}".format(permissionType.name)

    @pytest.mark.django_db
    def test_position_model(self):
        position = baker.make(Position)
        assert position.__str__() == "{}".format(position.name)

    @pytest.mark.django_db
    def test_vacation_model(self):
        vacation = baker.make(Vacation)
        assert vacation.__str__() == "{}".format(vacation.employee)
