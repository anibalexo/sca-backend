import pytest
from django.contrib.auth import get_user_model

User = get_user_model()

@pytest.mark.django_db
def test_create_user_custom():
    user = User.objects.create_user(email='test@gmail.com', password='123',
                                    is_staff=False)
    assert user.email == 'test@gmail.com'
    assert user.__str__() == 'test@gmail.com'

@pytest.mark.django_db
def test_create_user_custom_bad():
    with pytest.raises(ValueError, match='Email must be set'):
        User.objects.create_user(email=None, password='123', is_staff=False)

@pytest.mark.django_db
def test_create_superuser_custom():
    user = User.objects.create_superuser(email='admin@gmail.com', password='123')
    assert user.is_superuser

@pytest.mark.django_db
def test_create_superuser_custom_bad():
    with pytest.raises(ValueError, match="Superuser must have is_staff=True."):
        User.objects.create_superuser(email='admin@gmail.com', password='123',
                                      is_staff=False)
    with pytest.raises(ValueError, match="Superuser must have is_superuser=True."):
        User.objects.create_superuser(email='admin@gmail.com', password='123',
                                      is_superuser=False)
