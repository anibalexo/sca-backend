import pytest
from django.contrib.auth import get_user_model
from django.urls import reverse_lazy
from rest_framework import status
from rest_framework.test import APIClient

User = get_user_model()

@pytest.mark.django_db
def test_api_login_jwt():
    User.objects.create_user(
        email='example@gmail.com',
        first_name='Example First Name',
        last_name='Example Last Name',
        password='1234'
    )
    client = APIClient()
    response = client.post(reverse_lazy('auth-token'), {'email': 'example@gmail.com', 'password': '1234'})
    assert 'refresh' in response.data
    assert 'access' in response.data
    assert 'uid' in response.data
    assert 'first_name' in response.data
    assert 'last_name' in response.data
    assert response.status_code == status.HTTP_200_OK

